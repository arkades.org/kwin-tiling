/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2014 Dominik Kummer <dominik.kummer@chello.at>
based on spirallayout.js by Matthias Gottschlag

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


/**
 * Class which arranges the windows in a spiral with the largest window filling
 * the left half of the screen.
 */
function StackLayout(screenRectangle) {
	try {
		Layout.call(this, screenRectangle);
		this.name = "Stack"
	} catch(err) {
		print(err, "in StackLayout");
	}
	this.master = 0;
}


StackLayout.prototype = new Layout();
StackLayout.prototype.constructor = StackLayout;
StackLayout.prototype.name = "Stack";
// TODO: Add an image for the layout switcher
StackLayout.prototype.image = null;
StackLayout.prototype.resetTileSizes = function() {
	try {
		// Simply erase all tiles and recreate them to recompute the initial sizes
		var tileCount = this.tiles.length;
		this.tiles.length = 0;
		for (var i = 0; i < tileCount; i++) {
			this.addTile();
		}
	} catch(err) {
		print(err, "in StackLayout.resetTileSizes");
	}
};

StackLayout.prototype.addTile = function() {
	try {
		if (this.tiles.length == 0) {
			// The first tile fills the whole screen
			var rect = Qt.rect(this.screenRectangle.x,
							   this.screenRectangle.y,
							   this.screenRectangle.width,
							   this.screenRectangle.height);
			this._createTile(rect);
			return;
		} else {
			// Divide the screen width evenly between full-height tiles
			// FIXME: Screenrectangle for struts is weird
			var lastRect = this.tiles[0].rectangle;
			var newRect = Qt.rect(this.screenRectangle.x,
								  lastRect.y,
									this.screenRectangle.width,
									Math.floor((this.screenRectangle.height + this.screenRectangle.y) / (this.tiles.length + 1)));
			// FIXME: Try to keep ratio
			for (var i = 0; i < this.tiles.length; i++) { 
				var rect = this.tiles[i].rectangle;
				rect.y = newRect.y + newRect.height * i;
				rect.height = newRect.height;
				this.tiles[i].rectangle = rect;
			}
			// Adjust tile's width for rounding errors
			newRect.y = newRect.y + newRect.height * this.tiles.length;
			newRect.height = (this.screenRectangle.height + this.screenRectangle.y) - newRect.y;
			// TODO: Move this before setting ratio to simplify
			this._createTile(newRect);
		}
	} catch(err) {
		print(err, "in StackLayout.addTile");
	}
};

StackLayout.prototype.removeTile = function(tileIndex) {
	try {
		// Remove the array entry
		var oldrect = this.tiles[tileIndex].rectangle;
		this.tiles.splice(tileIndex, 1);
		// Update the other tiles
		if (this.tiles.length == 1) {
			this.tiles[0].rectangle = Qt.rect(this.screenRectangle.x,
											  this.screenRectangle.y,
											  this.screenRectangle.width,
											  this.screenRectangle.height);
			this.tiles[0].hasDirectNeighbour[Direction.Up] = false;
			this.tiles[0].hasDirectNeighbour[Direction.Down] = false;
		}
		if (this.tiles.length > 1) {
			var tileCount = this.tiles.length;
			var lastRect = this.tiles[0].rectangle;
			var newRect = Qt.rect(this.screenRectangle.x,
								  this.screenRectangle.y,
									this.screenRectangle.width,
								  Math.floor(this.screenRectangle.width / tileCount));
			var lowest = 1;
			for (var i = 0; i < this.tiles.length; i++) {
				var rect = this.tiles[i].rectangle;
				rect.y = newRect.y + newRect.height * i;
				rect.height = newRect.height;
				this.tiles[i].rectangle = rect;
				this.tiles[i].hasDirectNeighbour[Direction.Up] = (i > 0);
				this.tiles[i].hasDirectNeighbour[Direction.Down] = (i < this.tiles.length - 1);
				this.tiles[i].neighbours[Direction.Up] = i - 1;
				this.tiles[i].neighbours[Direction.Down] = i + 1;
			}
			// Adjust rightmost tile's height for rounding errors
			this.tiles[this.tiles.length - 1].rectangle.height = (this.screenRectangle.height + this.screenRectangle.y) - this.tiles[this.tiles.length - 1].rectangle.y;
		}
	} catch(err) {
		print(err, "in StackLayout.removeTile");
	}
};

StackLayout.prototype.resizeTile = function(tileIndex, rectangle) {
	try {
		// TODO: Mark tile as resized so it can keep its size on tileadd/remove
		if (tileIndex < 0 || tileIndex > this.tiles.length) {
			print("Tileindex invalid", tileIndex, "/", this.tiles.length);
			return;
		}
		var tile = this.tiles[tileIndex];
		if (tile == null) {
			print("No tile");
			return;
		}
		if (rectangle == null){
			print("No rect");
			return;
		}
		// Cap rectangle at screenedges
		var oldRect = tile.rectangle;
		if (rectangle.y < this.screenRectangle.y) {
			// Width can only change in reaction to the x change
			// As we only resize width and height, we can return here
			if (tileIndex == 0) {
				return;
			}
			rectangle.y = this.screenRectangle.y;
		}
		// First tile should not move away from left edge
		if (oldRect.y == this.screenRectangle.y && rectangle.y != oldRect.y) {
			if (tileIndex == 0) {
				return;
			}
		}
		if (rectangle.x + rectangle.width > this.screenRectangle.x + this.screenRectangle.width) {
			rectangle.width = this.screenRectangle.x + this.screenRectangle.width - rectangle.x;
		}
		if (rectangle.y + rectangle.height > this.screenRectangle.y + this.screenRectangle.height) {
			rectangle.height = this.screenRectangle.y + this.screenRectangle.height - rectangle.y;
		}
		if (tileIndex == this.tiles.length - 1) {
			rectangle.height = (this.screenRectangle.y + this.screenRectangle.height) - rectangle.y;
		}
		rectangle.x = this.screenRectangle.x;
		rectangle.width = this.screenRectangle.width;
		var newRect = Qt.rect(this.screenRectangle.x,
							  this.screenRectangle.y,
								this.screenRectangle.width,
								Math.floor((rectangle.y - this.screenRectangle.y) / tileIndex));
		for(i = 0; i < tileIndex; i++) {
			var rect = this.tiles[i].rectangle;
			rect.y = newRect.y + newRect.height * i;
			rect.height = newRect.height;
			this.tiles[i].rectangle = rect;
		}
		// Rightmost point - Right edge of resized window / number of tiles on the right side
		// Do rounding later to preserve accuracy
		newRect.height = ((this.screenRectangle.height + this.screenRectangle.y)
									- (rectangle.y + rectangle.height)) 
								   / (this.tiles.length - (tileIndex + 1));
		for(i = tileIndex + 1; i < this.tiles.length; i++){
			var rect = this.tiles[i].rectangle;
			rect.y = (rectangle.y + rectangle.height) + (i - tileIndex - 1) * newRect.height;
			rect.height = newRect.heigth;
			this.tiles[i].rectangle = rect;
		}
		rectangle.x = this.screenRectangle.x;
		rectangle.width = this.screenRectangle.width;
		this.tiles[tileIndex].rectangle = rectangle;
	} catch(err) {
		print(err, "in StackLayout.resizeTile");
	}
};

StackLayout.prototype._createTile = function(rect) {
	try {
		// Update the last tile in the list
		if (this.tiles.length > 0) {
			var lastTile = this.tiles[this.tiles.length - 1];
			lastTile.neighbours[Direction.Down] = this.tiles.length;
			lastTile.hasDirectNeighbour[Direction.Down] = true;
		}
		// Create a new tile and add it to the list
		var tile = {};
		tile.rectangle = rect;
		tile.neighbours = [];
		tile.hasDirectNeighbour = [];
		tile.neighbours[Direction.Up] = (this.tiles.length - 1);
		tile.hasDirectNeighbour[Direction.Up] = (this.tiles.length > 0);
		tile.neighbours[Direction.Down] = -1;
		tile.hasDirectNeighbour[Direction.Down] = false;
		tile.hasDirectNeighbour[Direction.Left] = false;
		tile.neighbours[Direction.Left] = -1;
		tile.neighbours[Direction.Right] = - 1;
		tile.hasDirectNeighbour[Direction.Right] = false;
		tile.index = this.tiles.length;
		this.tiles.push(tile);
	} catch(err) {
		print(err, "in StackLayout._createTile");
	}
};
