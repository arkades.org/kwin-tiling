/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2012 Mathias Gottschlag <mgottschlag@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

Qt.include("signal.js");
Qt.include("tile.js");
Qt.include("tilelist.js");
Qt.include("layout.js");
Qt.include("stacklayout.js");
Qt.include("spirallayout.js");
Qt.include("halflayout.js");
Qt.include("valflayout.js");
Qt.include("bladelayout.js");
Qt.include("tiling.js");
//Qt.include("tests.js");


/**
 * Class which manages all layouts, connects the various signals and handlers
 * and implements all keyboard shortcuts.
 * @class
 */
function TilingManager(parent) {
	
	this.id = 'tiling';
	this.parent = parent;
	this.init = true;
	this._sessionOpen = false;
	this._activityChange = false;
	this.onCompleted = new Signal();
	this.activities = [];
	
	/**
		* Default layout type which is selected for new layouts.
		*/
	this.defaultLayout = HalfLayout;

	/**
		* List of all available layout types.
		*/
	this.availableLayouts = [
		HalfLayout,
		BladeLayout,
		StackLayout,
		ValfLayout,
		SpiralLayout/*,
		ZigZagLayout,
		ColumnLayout,
		RowLayout,
		GridLayout,
		MaximizedLayout,
		FloatingLayout*/
	];
	
	/**
		* Array containing a list of layouts for every desktop. Each of the lists
		* has one element per screen and activity.
		*/
	this.layouts = [];
	/**
		* List of all tiles in the system.
		*/
	this.tiles = new TileList(this);
		/**
		* True if a user moving operation is in progress.
		*/
	this._moving = false;
	/**
		* The screen where the current window move operation started.
		*/
	this._movingStartScreen = 0;
	/**
	 * Whether tiling is active on all desktops
	 * This is overridden by per-desktop settings
	 */
	this.userActive = readConfig("userActive", true);
	
	/**
	 * Read Layout and Positions Configuration Values.
	 */
	this._readLayoutConf()
	this._readPositionsConf()

	var self = this;
	// Connect the tile list signals so that new tiles are added to the layouts
	this.tiles.tileAdded.connect(function(tile) {
		self._onTileAdded(tile);
	});
	this.tiles.tileRemoved.connect(function(tile) {
		self._onTileRemoved(tile);
	});
	
	workspace.sessionOpened.connect(function() {
		self._start();
	});
	
	registerShortcut("Tiling Tests",
									 "Tiling Tests",
									"Meta+T",
									function() {
										print('start');
										self._start()
									});
	
	this._createNotifier();
}


TilingManager.prototype._createNotifier = function() {
	print("createNotifier")
	try {
		this.component = Qt.createComponent("../../tiling/components/Notifier.qml");
		if (this.component.status == Component.Ready) {
			this._finishNotifier();
		} else {
			this.component.statusChanged.connect(this._finishNotifier);
		}
	} catch (err) {
		print(err, 'in TilingManager.prototype._createNotifier');
	}
}

TilingManager.prototype._finishNotifier = function() {
	print("finishNotifier")
	try {
		if (this.component.status == Component.Ready) {
			
			this.qmlNotifier = this.component.createObject(this.parent);
			print("qmlNotifier ready", this.qmlNotifier);
			if (this.qmlNotifier == null) {
				// Error Handling
				print("Error creating object");
			} 
		} else if (this.component.status == Component.Error) {
			// Error Handling
			print("Error loading component:", this.component.errorString());
		}
	} catch (err) {
		print(err, 'in TilingManager.prototype._finishNotifier');
	}
}


TilingManager.prototype._start = function() {
	try {
		this._sessionOpen = true;
		/**
		* Activities in the system.
		*/
		this.activities = workspace.activities;
		/**
		* Current Activity in the system.
		*/
		this._currentActivity = workspace.currentActivity;
		/**
		* Number of activities in the system.
		*/
		this.activityCount = this.activities.length;
		/**
		* Number of desktops in the system.
		*/
		this.desktopCount = workspace.desktopGridWidth
		* workspace.desktopGridHeight;
		/**
		* Number of screens in the system.
		*/
		this.screenCount = workspace.numScreens;
		/**
		* Current screen, needed to be able to track screen changes.
		*/
		this._currentScreen = workspace.activeScreen;
		/**
		* Current desktop, needed to be able to track screen changes.
		*/
		this._currentDesktop = workspace.currentDesktop - 1;
		
		
		this._registerCallbacks();
		this._registerShortcuts();
		
		// Create the various layouts, one for every desktop
		for (var i = 0; i < this.desktopCount; i++) {
			this._createDefaultLayouts(i);
		}
		
		var existingClients = workspace.clientList();
		for (var i=0; i < existingClients.length; i++) {
			this.tiles.addClient(existingClients[i]);
		}
		this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].activate();
		this._init = false;
		this.onCompleted.emit();
	} catch (err) {
		print(err, 'in TilingManager.start');
	}
};


TilingManager.prototype._registerCallbacks = function() {
	// Register global callbacks
	var self = this;
	workspace.numberDesktopsChanged.connect(function() {
		self._onNumberDesktopsChanged();
	});
	workspace.numberScreensChanged.connect(function() {
		self._onNumberScreensChanged();
	});
	workspace.activitiesChanged.connect(function(activity) {
		self._onNumberActivitiesChanged(activity);
	});
	workspace.screenResized.connect(function(screen) {
		try {
			self._onScreenResized(screen);
		} catch(err) {
			print(err);
		}
	});
	workspace.currentDesktopChanged.connect(function() {
		self._onCurrentDesktopChanged();
	});
	workspace.currentActivityChanged.connect(function(activity) {
		self._onCurrentActivityChanged(activity);
	});
}


TilingManager.prototype._registerShortcuts = function() {
	// Register keyboard shortcuts
	registerShortcut("Next Tiling Layout",
									 "Next Tiling Layout",
									"Meta+PgDown",
									function() {
										var currentLayout = self._getCurrentLayoutType();
										var nextIndex = (currentLayout.index + 1) % self.availableLayouts.length;
										self._switchLayout(workspace.currentDesktop - 1,
																			 workspace.activeScreen,
														 self._currentActivity,
														 nextIndex);
									});
	registerShortcut("Previous Tiling Layout",
									 "Previous Tiling Layout",
									"Meta+PgUp",
									function() {
										var currentLayout = self._getCurrentLayoutType();
										var nextIndex = currentLayout.index - 1;
										if (nextIndex < 0) {
											nextIndex += self.availableLayouts.length;
										}
										self._switchLayout(workspace.currentDesktop - 1,
																			 workspace.activeScreen,
														 self._currentActivity,
														 nextIndex);
									});
	registerShortcut("Toggle Floating",
									 "Toggle Floating",
									"Meta+F",
									function() {
										var client = workspace.activeClient;
										if (client == null) {
											print("No active client");
											return;
										}
										client.tiling_floating = !client.tiling_floating;
										if (client.tiling_floating == true) {
											self.tiles._onClientRemoved(client);
										} else {
											self.tiles.addClient(client);
										}
									});
	registerShortcut("Toggle Border",
									 "Toggle Border",
									"Meta+U",
									function() {
										var client = workspace.activeClient;
										if (client == null) {
											print("No active client");
											return;
										}
										client.noBorder = ! client.noBorder;
										if (client.noBorder == false) {
											client.tiling_floating = true;
											self.tiles._onClientRemoved(client);
										} else {
											client.tiling_floating = false;
											self.tiles.addClient(client);
										}
									});
	registerShortcut("Toggle Border for all",
									 "Toggle Border for all",
									"Meta+Shift+U",
									function() {
										self.tiles.toggleNoBorder()
									});
	registerShortcut("Switch Focus Left",
									 "Switch Focus Left",
									"Meta+H",
									function() {
										self._switchFocus(Direction.Left);
									});
	registerShortcut("Switch Focus Right",
									 "Switch Focus Right",
									"Meta+L",
									function() {
										self._switchFocus(Direction.Right);
									});
	registerShortcut("Switch Focus Up",
									 "Switch Focus Up",
									"Meta+K",
									function() {
										self._switchFocus(Direction.Up);
									});
	registerShortcut("Switch Focus Down",
									 "Switch Focus Down",
									"Meta+J",
									function() {
										self._switchFocus(Direction.Down);
									});
	registerShortcut("Move Window Left",
									 "Move Window Left",
									"Meta+Shift+H",
									function() {
										self._moveTile(Direction.Left);
									});
	registerShortcut("Move Window Right",
									 "Move Window Right",
									"Meta+Shift+L",
									function() {
										self._moveTile(Direction.Right);
									});
	registerShortcut("Move Window Up",
									 "Move Window Up",
									"Meta+Shift+K",
									function() {
										self._moveTile(Direction.Up);
									});
	registerShortcut("Move Window Down",
									 "Move Window Down",
									"Meta+Shift+J",
									function() {
										self._moveTile(Direction.Down);
									});
	registerShortcut("Toggle Tiling",
									 "Toggle Tiling",
									"Meta+Shift+f11",
									function() {
										self.layouts[workspace.currentDesktop - 1][workspace.activeScreen][self._currentActivity].toggleUserActive();
									});
	registerShortcut("Swap Window With Master",
									 "Swap Window With Master",
									"Meta+Shift+M",
									function() {
										try {
											var layout = self.layouts[workspace.currentDesktop - 1][workspace.activeScreen][self._currentActivity];
											if (layout != null) {
												var client = workspace.activeClient;
												if (client != null) {
													var tile = layout.getTile(client.x, client.y);
												}
											}
											if (tile != null) {
												layout.swapTiles(tile, layout.tiles[0]);
											}
										} catch(err) {
											print(err, "in swap-window-with-master");
										}
									});
	registerShortcut("Resize Active Window To The Left",
									 "Resize Active Window To The Left",
									"Meta+Alt+H",
									function() {
										try {
											var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
											// Qt.rect automatically clamps negative values to zero
											geom = Qt.rect(tile.rectangle.x - 10,
																		 tile.rectangle.y,
													tile.rectangle.width - 10,
													tile.rectangle.height);
											if (geom.x < 0) {
												geom.x = 0;
											}
											self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
										} catch(err) {
											print(err, "in resize-window-to-the-left");
										}
									});
	registerShortcut("Resize Active Window To The Right",
									 "Resize Active Window To The Right",
									"Meta+Alt+L",
									function() {
										try {
											var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
											// Qt.rect automatically clamps negative values to zero
											geom = Qt.rect(tile.rectangle.x,
																		 tile.rectangle.y,
													tile.rectangle.width + 10,
													tile.rectangle.height);
											self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
										} catch(err) {
											print(err, "in resize-window-to-the-left");
										}
									});
	registerShortcut("Resize Active Window To The Top",
									 "Resize Active Window To The Top",
									"Meta+Alt+K",
									function() {
										try {
											var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
											// Qt.rect automatically clamps negative values to zero
											geom = Qt.rect(tile.rectangle.x,
																		 tile.rectangle.y - 10,
													tile.rectangle.width,
													tile.rectangle.height + 10);
											self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
										} catch(err) {
											print(err, "in resize-window-to-the-left");
										}
									});
	registerShortcut("Resize Active Window To The Bottom",
									 "Resize Active Window To The Bottom",
									"Meta+Alt+J",
									function() {
										try {
											var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
											// Qt.rect automatically clamps negative values to zero
											geom = Qt.rect(tile.rectangle.x,
																		 tile.rectangle.y,
													tile.rectangle.width,
													tile.rectangle.height - 10);
											self.layouts[tile._currentDesktop - 1][tile._currentScreen].resizeMaster(geom);
										} catch(err) {
											print(err, "in resize-window-to-the-left");
										}
									});
	registerUserActionsMenu(function(client) {
		return {
			text : "Toggle floating",
			triggered: function () {
				client.tiling_floating = ! client.tiling_floating;
				if (client.tiling_floating == true) {
					self.tiles._onClientRemoved(client);
				} else {
					self.tiles.addClient(client);
				}
			}
		};
	});
}


TilingManager.prototype._createDefaultLayouts = function(desktop) {
	try {
		var screenLayouts = [];
		var activityLayouts = [];
		var i = desktop;
		var self = this;
		for (var j = 0; j < this.screenCount; j++) {
			for (var k = 0; k < this.activityCount; k++) {
				if (typeof(this.layoutConfig[i]) !== 'undefined' && 
						typeof(this.layoutConfig[i][j]) !== 'undefined' && 
						typeof(this.layoutConfig[i][j][k]) !== 'undefined') {
					var userConfig = true;
					var layout = this.layoutConfig[i][j][k].layout;
					var tiling = this.layoutConfig[i][j][k].tiling;
				} else {
					var layout = this.defaultLayout;
					var tiling = false;
					var userConfig = false;
				}
				
				activityLayouts[k] = new Tiling(layout, i, j, k);
				activityLayouts[k].tilingActivated.connect(function(tiling) {
					self._onTilingActivated(tiling);
				});
		// Either the default is to tile and the desktop hasn't been configured,
		// or the desktop has been set to tile (in which case the default is irrelevant)
				activityLayouts[k].userActive = (this.userActive == true && userConfig == false) || (tiling == true);
				print('TilingManager._createDefaultLayouts', i, j, k);
			}
			screenLayouts[j] = activityLayouts
		}
		this.layouts[i] = screenLayouts;
	} catch (err) {
		print(err, 'in TilingManager._createDefaultLayouts');
	}
};

TilingManager.prototype._getCurrentLayoutType = function() {
	try {
		var currentLayout = this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity];
		return currentLayout.layoutType;
	} catch (err) {
		print(err, 'in TilingManager._getCurrentLayoutType');
	}
};

TilingManager.prototype._onTileAdded = function(tile) {
    // Add tile callbacks which are needed to move the tile between different
    // screens/desktops
	try {
		var self = this;
		tile.captionChanged.connect(function(oldID, newID) {
			self._onTileCaptionChanged(tile, oldID, newID);
		});
		tile.screenChanged.connect(function(oldScreen, newScreen) {
			self._onTileScreenChanged(tile, oldScreen, newScreen);
		});
		tile.desktopChanged.connect(function(oldDesktop, newDesktop) {
			self._onTileDesktopChanged(tile, oldDesktop, newDesktop);
		});
		tile.activityChanged.connect(function(oldActivity, newActivity) {
			self._onTileActivityChanged(tile, oldActivity, newActivity);
		});
		tile.positionSet.connect(function(oldID, newID) {
			self._onTilePositionSet(tile, oldID, newID);
		});
		tile.positionChanged.connect(function(client) {
			self._onTilePositionChanged(client);
		});
		tile.movingStarted.connect(function() {
			self._onTileMovingStarted(tile);
		});
		tile.movingEnded.connect(function() {
			self._onTileMovingEnded(tile);
		});
		tile.resizingEnded.connect(function() {
			self._onTileResized(tile);
		});
		// Add the tile to the layouts
		for (var i = 0; i < tile.desktops.length; i++) {
			for (var j = 0; j < tile.screens.length; j++) {
				for (var k = 0; k < tile.activities.length; k++) {
					var tileLayouts = this._getLayouts(tile.desktops[i], tile.screens[j], tile.activities[k]);
					tileLayouts.forEach(function(layout) {
						layout.addTile(tile);
					});
				}
			}
		}
		
		if (tile.desktops.indexOf(this._currentDesktop) == -1) {
			this._currentDesktop = tile.desktops[0];
		}
		if (tile.screens.indexOf(this._currentScreen) == -1) {
			this._currentScreen = tile.screens[0];
		}
		if (tile.activities.indexOf(this._currentActivity) == -1) {
			this._currentActivity = tile.activities[0];
		}
		if (this.init == false) {
			this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].activate();
		}
		this.qmlNotifier.tileAdded(tile);
	} catch (err) {
		print(err, 'in TilingManager._onTileAdded');
	}
};

TilingManager.prototype._onTileResized = function(tile) {
	try {
		var tileLayouts = this._getLayouts(this._currentDesktop, this._currentScreen, this._currentActivity);
		tileLayouts.forEach(function(layout) {
			layout.resizeTile(tile);
		});
		this.qmlNotifier.tileResized(tile);
	} catch (err) {
		print(err, 'in TilingManager._onTileResized');
	}
};

TilingManager.prototype._getMaster = function(screen, desktop, activity) {
	try {
		return this.layouts[desktop][screen][activity].getMaster();
	} catch(err) {
		print(err, "in TilingManager._getMaster");
	}
}

TilingManager.prototype._onTileRemoved = function(tile) {
	try {
		this.qmlNotifier.tileRemoved(tile);
		var tileLayouts = this._getLayouts(tile.desktops, tile.screens, tile.activities);
		tileLayouts.forEach(function(layout) {
			layout.removeTile(tile);
		});
	} catch(err) {
		print(err, "in TilingManager._onTileRemoved");
	}
};

TilingManager.prototype._onNumberDesktopsChanged = function() {
	try {
		var newDesktopCount =
			workspace.desktopGridWidth * workspace.desktopGridHeight;
		var onAllDesktops = tiles.tiles.filter(function(tile) {
			return (tile.desktop == -1 || tile.desktops.indexOf(newDesktopCount-1) > -1);
		});
		// Remove tiles from desktops which do not exist any more (we only have to
		// care about tiles shown on all desktops as all others have been moved away
		// from the desktops by kwin before)
		onAllDesktops.forEach(function(tile) {
			for (var i = tile.desktops.indexOf(newDesktopCount); i < tile.desktops.length; i++) {
				for (var j = 0; j < tile.screens.length; j++) {
					for (var k = 0; k < tile.activities.length; k++) {
						this.layouts[tile.desktops[i]][tile.screens[j]][tile.activities[k]].removeTile(tile);
					}
				}
			}
		});
		
		// Add new desktops
		for (var i = this.desktopCount; i < newDesktopCount; i++) {
			this._createDefaultLayouts(i);
			onAllDesktops.forEach(function(tile) {
				for (var j = 0; j < tile.screens.length; j++) {
					for (var k = 0; k < tile.activities.length; k++) {
						this.layouts[i][tile.screens[j]][tile.activities[k]].addTile(tile);
					}
				}	
			});
		}
		// Remove deleted desktops
		if (this.desktopCount > newDesktopCount) {
			this.layouts.length = newDesktopCount;
		}
		this.desktopCount = newDesktopCount;
		this._onCurrentDesktopChanged();
		
	} catch (err) {
		print(err, 'in TilingManager._onNumberDesktopsChanged');
	}
};

TilingManager.prototype._onNumberScreensChanged = function() {
	// Add new screens
	try {
		var newSceenCount = workspace.numScreens;
		var onAllDesktops = tiles.tiles.filter(function(tile) {
			return (tile.screen == -1 || tile.screens.indexOf(newScreenCount-1) > -1);
		});
		
		
		onAllDesktops.forEach(function(tile) {
			for (var i = tile.screens.indexOf(newScreenCount-1); i < tile.screens.length; i++) {
				for (var j = 0; j < tile.desktops.length; j++) {
					for (var k = 0; k < tile.activities.length; k++) {
						this.layouts[tile.desktops[j]][tile.screens[i]][tile.activities[k]].removeTile(tile);
					}
				}
			}
		});
		
		// Add new screens
		if (this.screenCount < workspace.numScreens) {
			for (var i = 0; i < this.desktopCount; i++) {
				for (var j = this.screenCount; j < workspace.numScreens; j++) {
					for (var k = this.activityCount; k < this.activities.length; k++) {
						this.layouts[i][j][k] = new Tiling(this.defaultLayout, i, j, k);
					}
				}
			}
		}
		
		for (var i = this.screenCount; i < newScreenCount; i++) {
			onAllDesktops.forEach(function(tile) {
				for (var j = 0; j < tile.desktops.length; j++) {
					for (var k = 0; k < tile.activities.length; k++) {
						this.layouts[tile.desktops[j]][tile.screens[i]][tile.activities[k]].addTile(tile);
					}
				}	
			});
		}
		
		// Remove deleted screens
		if (this.screenCount > workspace.numScreens) {
			for (var i = 0; i < this.desktopCount; i++) {
				this.layouts[i].length = workspace.numScreens;
			}
		}
		// Remove deleted activities
		if (this.activityCount > this.activities.length) {
			for (var i = 0; i < this.desktopCount; i++) {
				for (var j = 0; j < this.screenCount; j++) {
					this.layouts[i][j].length = this.activities.length;
				}
			}
		}
		this.activityCount = this.activities.length;
		this.screenCount = workspace.numScreens;
		this._onCurrentScreenChanged();
		
	} catch (err) {
		print(err, 'in TilingManager._onNumberScreensChanged');
	}
};

TilingManager.prototype._onNumberActivitiesChanged = function(activity) {
	// Add new activities
	try {
		this.activities = this.dataengine.sources.slice(0,activities.sources.length-1);
		newActivityCount = this.activities.length;
		
		var onAllDesktops = tiles.tiles.filter(function(tile) {
			return (tile.activities.indexOf(newActivityCount-1) > -1);
		});
		
		if (this.activityCount < this.activities.length) {
			for (var i = 0; i < this.desktopCount; i++) {
				for (var j = this.screenCount; j < workspace.numScreens; j++) {
					for (var k = this.activityCount; k < this.activities.length; k++) {
						this.layouts[i][j][k] = new Tiling(this.defaultLayout, i, j, k);
					}
				}
			}
		}
		
		// Remove deleted screens
		if (this.screenCount > workspace.numScreens) {
			for (var i = 0; i < this.desktopCount; i++) {
				this.layouts[i].length = workspace.numScreens;
			}
		}
		// Remove deleted activities
		if (this.activityCount > this.activities.length) {
			for (var i = 0; i < this.desktopCount; i++) {
				for (var j = 0; j < this.screenCount; j++) {
					this.layouts[i][j].length = this.activities.length - 1;
				}
			}
		}
		this._currentActivity = this.activities.indexOf(activity);
		this.activityCount = this.activities.length;
		this.screenCount = workspace.numScreens;
		this._onCurrentActivityChanged();
	} catch (err) {
		print(err, 'in TilingManager._onNumberActivitiesChanged');
	}
};

TilingManager.prototype._onScreenResized = function(screen) {
	try {
		if (screen < this.screenCount) {
			for (var i = 0; i < this.desktopCount; i++) {
				for (var k = 0; i < this.activityCount; k++) {
					// TODO
					this.layouts[i][screen][k].activate();
				}
			}
		}
	} catch (err) {
		print(err, 'in TilingManager._onScreenResized');
	}
};

TilingManager.prototype._onTileActivityChanged = function(tile, oldActivity, newActivity) {
	// If a tile is moved by the user, screen changes are handled in the move
	// callbacks below
	try {
		if (this._moving) {
			return;
		}
		var oldLayouts = this._getLayouts(tile.desktops, tile.screens, oldActivity);
		var newLayouts = this._getLayouts(tile.desktops, tile.screens, newActivity);
		this._changeTileLayouts(tile, oldLayouts, newLayouts);
		this.qmlNotifier.tileActivityChanged(tile);
	} catch(err) {
		print(err, "in TilingManager._onTileDesktopChanged");
	}
	
};

TilingManager.prototype._onTileScreenChanged = function(tile, oldScreen, newScreen) {
		// If a tile is moved by the user, screen changes are handled in the move
		// callbacks below
	try {
		if (this._moving) {
			return;
		}
		var oldLayouts = this._getLayouts(tile.desktops, oldScreen, tile.activities);
		var newLayouts = this._getLayouts(tile.desktops, newScreen, tile.activities);
		this._changeTileLayouts(tile, oldLayouts, newLayouts);
		this.qmlNotifier.tileScreenChanged(tile);
	} catch (err) {
		print(err, 'in TilingManager._onTileScreenChanged');
	}
};

TilingManager.prototype._onTileDesktopChanged = function(tile, oldDesktop, newDesktop) {
		try {
			var oldLayouts = this._getLayouts(oldDesktop, tile.screens, tile.activities);
			var newLayouts = this._getLayouts(newDesktop, tile.screens, tile.activities);
			this._changeTileLayouts(tile, oldLayouts, newLayouts);
		} catch(err) {
			print(err, "in TilingManager._onTileDesktopChanged");
		}
	};
	
TilingManager.prototype._onTileCaptionChanged = function(tile, oldID, newID) {
	try {
		this._changeTileIdConfig(tile, oldID, newID);
		this.qmlNotifier.tileCaptionChanged(tile);
	} catch(err) {
		print(err, "in TilingManager._onTileCaptionChanged");
	}
};
	
TilingManager.prototype._onTilePositionSet = function(tile, oldID, newID) {
	try {
		this._changeTilePosConfig(tile, oldID, newID);
		this.qmlNotifier.tilePositionSet(tile);
	} catch(err) {
		print(err, "in TilingManager._onTilePositionChanged");
	}
};

TilingManager.prototype._onTilePositionChanged = function(client) {
	try {
		this.qmlNotifier.tilePositionChanged(client);
	} catch(err) {
		print(err, "in TilingManager._onTilePositionChanged");
	}
};

TilingManager.prototype._onTileMovingStarted = function(tile) {
	// NOTE: This supports only one moving window, breaks with multitouch input
	try {
		this._moving = true;
		this._movingStartScreen = tile.clients[0].screen;
	} catch (err) {
		print(err, 'in TilingManager._onTileMovingStarted');
	}
};

TilingManager.prototype._onTileMovingEnded = function(tile) {
	try {
		var client = tile.clients[0];
		this._moving = false;
		var movingEndScreen = client.screen;
		var windowRect = client.geometry;
		if (client.tiling_tileIndex >= 0) {
			if (this._movingStartScreen != movingEndScreen) {
				// Transfer the tile from one layout to another layout
				var startLayout = this.layouts[this._currentDesktop][this._movingStartScreen][this._currentActivity];
				var endLayout = this.layouts[this._currentDesktop][client.screen][this._currentActivity];
				startLayout.removeTile(tile);
				endLayout.addTile(tile, windowRect.x + windowRect.width / 2,
								  windowRect.y + windowRect.height / 2);
			} else {
				// Transfer the tile to a different location in the same layout
				var layout = this.layouts[this._currentDesktop][client.screen][this._currentActivity];
				var targetTile = layout.getTile(windowRect.x + windowRect.width / 2,
												windowRect.y + windowRect.height / 2);
				// In case no tile is found (e.g. middle of the window is offscreen), move the client back
				if (targetTile == null) {
					targetTile = tile;
				}
				// swapTiles() works correctly even if tile == targetTile
				layout.swapTiles(tile, targetTile);
			}
		}
	} catch(err) {
		print(err, "in TilingManager._onTileMovingEnded");
	}
};


TilingManager.prototype._onTilingActivated = function(tiling) {
	try {
		tiling._updateAllTilePositions();
		
		this._currentScreen = workspace.activeScreen;
		this._currentActivity = callDBus("org.kde.ActivityManager","/ActivityManager/Activities","org.kde.ActivityManager.Activities","CurrentActivity");
		this._currentDesktop = workspace.currentDesktop-1;
		if (this._currentActivity != tiling.activity) {
			this._currentActivity = tiling.activity;
			callDBus("org.kde.ActivityManager","/ActivityManager/Activities","org.kde.ActivityManager.Activities","SetCurrentActivity", this.activities[this._currentActivity]);
			this.activityChange = false;
		}
		if (this._currentDesktop != tiling.desktop) {
			this._currentDesktop = tiling.desktop;
			workspace.currentDesktop = this._currentDesktop+1;
			this.desktopChange = false;
		}
		if (this._currentScreen != tiling.screen) {
			this._currentScreen = tiling.screen;
			workspace.activeScreen =  tiling.screen;
		}
		
		this.qmlNotifier.tilingActivated(tiling);
		this._writePositionsConf();
	} catch (err) {
		print(err, 'in TilingManager._switchActivity()');
	}
};


TilingManager.prototype._changeTileIdConfig = function(tile, oldID, newID) {
	try {
		if (this.tiles.positionConfig[oldID] !== undefined) {
			oldConf = this.tiles.positionConfig[oldID];
			var tileposition = {};
			tileposition.count = oldConf.count;
			tileposition.wid = oldConf.wid;
			tileposition.desktops = oldConf.desktops;
			tileposition.activities = oldConf.activities;
			tileposition.screens = oldConf.screens;
			tileposition.tileindex = oldConf.tileIndex;
			this.tiles.positionConfig[newID] = tileposition;
			delete this.tiles.positionConfig[oldID];
		} else {
			var tileposition = {};
			tileposition.count = 0;
			tileposition.wid = tile.clients[0].windowId;
			tileposition.desktops = tile.desktops;
			tileposition.activities = tile.activities;
			tileposition.screens = tile.screens;
			tileposition.tileindex = tile.tileIndex;
			this.tiles.positionConfig[newID] = tileposition;
		}
		
		this._writePositionsConf();
	} catch (err) {
		print(err, 'in TilingManager._changeTileConfig');
	}
};

TilingManager.prototype._changeTilePosConfig = function(tile, oldID, newID) {
	try {
		var tileposition = {};
		tileposition.count = 0;
		tileposition.wid = tile.clients[0].windowId;
		tileposition.desktops = tile.desktops;
		tileposition.activities = tile.activities;
		tileposition.screens = tile.screens;
		tileposition.tileindex = tile.tileIndex;
		this.tiles.positionConfig[newID] = tileposition;
		
		if (this.tiles.positionConfig[oldID] !== undefined) {
			delete this.tiles.positionConfig[oldID];
		} 
	} catch (err) {
		print(err, 'in TilingManager._changeTileConfig');
	}
};

TilingManager.prototype._changeTileLayouts = function(tile, oldLayouts, newLayouts) {
	try {
		oldLayouts.forEach(function(layout) {
			layout.removeTile(tile);
		});
		newLayouts.forEach(function(layout) {
			layout.addTile(tile);
		});
	} catch(err) {
		print(err, "in TilingManager._changeTileLayouts");
	}
};

TilingManager.prototype._onCurrentDesktopChanged = function() {
	try {
		if (this._currentDesktop == workspace.currentDesktop-1) {
			return;
		}
		if (TilingManager._activityChange == false) {
			if (this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
				this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].deactivate();
			}
			this._currentDesktop = workspace.currentDesktop - 1;
			if (! this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
				this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].activate();
			}
		} else {
			workspace.currentDesktop = this._currentDesktop+1;
			TilingManager._activityChange = false;
		}
		this.qmlNotifier.currentDesktopChanged(this._currentDesktop+1);
	} catch (err) {
		print(err, 'in TilingManager._onCurrentDesktopChanged');
	}
	
};

TilingManager.prototype._onCurrentScreenChanged = function() {
	try {
		if (this._currentScreen == workspace.activeScreen) {
			return;
		}
		// TODO: This is wrong, we need to activate *all* visible layouts
		if (this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
			this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].deactivate();
		}
		this._currentScreen = workspace.activeScreen;
		if (! this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
			this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].activate();
		}
		this.qmlNotifier.currentScreenChanged(this._currentScreen);
	} catch (err) {
		print(err, 'in TilingManager._onCurrentScreenChanged');
	}
};

TilingManager.prototype._onCurrentActivityChanged = function(activity) {
	try {
		if (this._currentActivity == this.activities.indexOf(activity)) {
			return;
		}
		
		TilingManager._activityChange = true;
		if (this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
			this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].deactivate();
		}
		this._currentActivity = this.activities.indexOf(activity);
		if (! this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].active) {
			this.layouts[this._currentDesktop][this._currentScreen][this._currentActivity].activate();
		}
		this.qmlNotifier.currentActivityChanged(activity);
	} catch (err) {
		print(err, 'in TilingManager._onCurrentActivityChanged');
	}
	
};

TilingManager.prototype._readLayoutConf = function() {
	// Read layout configuration
	// Format: desktop:layoutname[,...]
	// Negative desktop number deactivates tiling
	
	try {
		for (var i = 0; i < this.availableLayouts.length; i++) {
			this.availableLayouts[i].index = i;
		}
		
		
		this.layoutConfig = [];
		var lC = String(readConfig("layouts", "")).replace(/ /g,"").split(",");
		for (var i = 0; i < lC.length; i++) {
			var layout = lC[i].split(":");
			try {
				var desktop = parseInt(layout[0]);
			} catch (err) {
				continue;
			}
			try {
				var screen = parseInt(layout[1]);
			} catch (err) {
				continue;
			}
			try {
				var activity = parseInt(layout[2]);
			} catch (err) {
				continue;
			}
			var l = this.defaultLayout;
			for (var j = 0; j < this.availableLayouts.length; j++) {
				if (this.availableLayouts[j].name == layout[3]) {
					l = this.availableLayouts[j];
				}
			}
			if (desktop < 0) {
				var tiling = false;
				desktop = desktop * -1;
			} else {
				var tiling = true;
			}
			if (desktop == 0) {
				this.defaultLayout = l;
			}
			// Subtract 1 because the config is in user-indexed format
			desktop = desktop - 1;
			var desktoplayout = {};
			
			desktoplayout.desktop = desktop;
			desktoplayout.activity = activity;
			desktoplayout.screen = screen;
			desktoplayout.layout = l;
			desktoplayout.tiling = tiling;
			if (Object.prototype.toString.call( this.layoutConfig[desktop] ) !== '[object Array]') {
				this.layoutConfig[desktop] = [];
			}
			if (Object.prototype.toString.call( this.layoutConfig[desktop][screen] ) !== '[object Array]') {
				this.layoutConfig[desktop][screen] = [];
			}
			
			this.layoutConfig[desktop][screen][activity] = desktoplayout;
		}
	} catch (err) {
		print(err, 'in TilingManager._readLayoutConf');
	}
};

TilingManager.prototype._readPositionsConf = function() {
	try {
		var pC = String(readConfig("positions", "")).replace(/ /g,"").split(",");
		for (var i = 0; i < pC.length; i++) {
			var position = pC[i].split(";");
			try {
				var id = position[0];
			} catch (err) {
				continue;
			}
			try {
				var desktops = position[1].split(':');
			} catch (err) {
				continue;
			}
			try {
				var screens = position[2].split(':');
			} catch (err) {
				continue;
			}
			try {
				var activities = position[3].split(':');
			} catch (err) {
				continue;
			}
			try {
				var tileIndex = position[4];
			} catch (err) {
				continue;
			}
			
			var tileposition = {};
			tileposition.desktops = desktops;
			tileposition.activities = activities;
			tileposition.screens = screens;
			tileposition.tileIndex = tileIndex;
			this.tiles.positionConfig[id] = tileposition;
		}
	} catch (err) {
		print(err, 'in TilingManager._readPositionsConf');
	}
};

TilingManager.prototype._writeLayoutConf = function() {
	try {
		var write = [];
		for (var i = 1; i <= this.desktopCount; i++) {
			for (var j = 0; j < this.screenCount; j++) {
				for (var k = 0; k < this.activityCount; k++) {
					entry = [i, j, k, [this.layouts[i-1][j][k].layout.name, 'Layout'].join('')].join(':');
					write.push(entry);
				}
			}
		}
		writeConfig('layouts', write.join());
	} catch (err) {
		print(err, 'in TilingManager._writeLayoutConf');
	}
};

TilingManager.prototype._writePositionsConf = function() {
	try {
		var write = [];
		for (ids in this.tiles.positionConfig) {
			pC = this.tiles.positionConfig[ids];
			write.push([ids, pC.desktops.join(':'), pC.screens.join(':'), pC.activities.join(':'), pC.tileIndex].join(';'))
		}
		
		writeConfig('positions',""); // write.join());
	} catch (err) {
		print(err, 'in TilingManager._writePositionsConf');
	}
};

TilingManager.prototype._switchLayout = function(desktop, screen, activity, layoutIndex) {
	// TODO: Show the layout switcher dialog
	try {
		print('TilingManager._switchLayout', desktop, screen, activity);
		var layoutType = this.availableLayouts[layoutIndex];
		this.layouts[desktop][screen][activity].setLayoutType(layoutType);
		this._writeLayoutConf();
	} catch (err) {
		print(err, 'in TilingManager._switchLayout');
	}
};



TilingManager.prototype._switchFocus = function(direction) {
	try {
		var client = workspace.activeClient;
		if (client == null) {
			return;
		}
		var activeTile = this.tiles.getTile(client);
		if (activeTile == null) {
			return;
		}
		var layout = this.layouts[client.desktop - 1][this._currentScreen][this._currentActivity];
		var nextTile = layout.getAdjacentTile(activeTile, direction, false);
		if (nextTile != null && nextTile != activeTile) {
			workspace.activeClient = nextTile.getActiveClient();
		}
	} catch (err) {
		print(err, 'in TilingManager._switchFocus');
	}
};

TilingManager.prototype._moveTile = function(direction) {
	try {
		var client = workspace.activeClient;
		if (client == null) {
			return;
		}
		var activeTile = this.tiles.getTile(client);
		if (activeTile == null) {
			print("Tile is floating");
			return;
		}
		var layout = this.layouts[client.desktop - 1][this._currentScreen][this._currentActivity];
		var nextTile = layout.getAdjacentTile(activeTile, direction, true);
		if (nextTile != null && nextTile != activeTile) {
			layout.swapTiles(activeTile, nextTile);
		}
	} catch (err) {
		print(err, 'in TilingManager._moveTile');
	}
};


TilingManager.prototype._getLayouts = function(desktops, screens, activities) {
	try {
		if (Object.prototype.toString.call( desktops ) !== '[object Array]') {
			desktops=[desktops];
		}
		if (Object.prototype.toString.call( screens ) !== '[object Array]') {
			screens=[screens];
		}
		if (Object.prototype.toString.call( activities ) !== '[object Array]') {
			activities=[activities];
		}
		
		result=[];
		for (var i = 0; i < desktops.length; i++) {
			for (var j = 0; j < screens.length; j++) {
				for (var k = 0; k < activities.length; k++) {
					if (desktops[i] > -1) {
						result.push(this.layouts[desktops[i]][screens[j]][activities[k]]);
					} else if (desktops[i] == -1) {
						for (var l = 0; l < this.desktopCount; l++) {
							result.push(this.layouts[l][screens[j]][activities[k]]);
						}
					}
				}
			}
		}
		return result;
	} catch (err) {
		print(err, 'in TilingManager._getLayouts');
	}
};
