/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2012 Mathias Gottschlag <mgottschlag@gmail.com>
Copyright (C) 2013 Fabian Homborg <fhomborg@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

/**
 * Class which manages the windows in one tile and handles resize/move and
 * property change events.
 * @class
 */
function Tile(parent, firstClient, tileIndex) {
	try {
		this.parent = parent;
		this.positionConfig = this.parent.positionConfig;
		/**
		 * List of the clients in this tile.
		 */
		this.clients = [firstClient];
		
		/**
		 * Signal which is triggered whenever tiles' position changes.
		 */
		this.positionChanged = new Signal();
		this.positionSet = new Signal();
		/**
		 * Signal which is triggered whenever the caption of the client changes.
		 */
		this.captionChanged = new Signal();
		/**
		 * Signal which is triggered whenever the user starts to move the tile.
		 */
		this.movingStarted = new Signal();
		/**
		 * Signal which is triggered whenever the user stops moving the tile.
		 */
		this.movingEnded = new Signal();
		/**
		 * Signal which is triggered whenever the geometry changes between
		 * movingStarted and movingEnded.
		 */
		this.movingStep = new Signal();
		/**
		 * Signal which is triggered whenever the user starts to resize the tile.
		 */
		this.resizingStarted = new Signal();
		/**
		 * Signal which is triggered whenever the user stops resizing the tile.
		 */
		this.resizingEnded = new Signal();
		/**
		 * Signal which is triggered whenever the geometry changes between
		 * resizingStarted and resizingEnded.
		 */
		this.resizingStep = new Signal();
		/**
		 * Signal which is triggered whenever the tile is moved to a different
		 * screen. Two parameters are passed to the handlers, the old and the new
		 * screen.
		 */
		this.screenChanged = new Signal();
		/**
		 * Signal which is triggered whenever the tile is moved to a different
		 * desktop. Two parameters are passed to the handlers, the old and the new
		 * desktop.
		 */
		this.desktopChanged = new Signal();
		/**
		 * Signal which is triggered whenever the tile is moved to a different
		 * activity. Two parameters are passed to the handlers, the old and the new
		 * activity.
		 */
		this.activityChanged = new Signal();
		
		/**
		 * Index of this tile in the TileList to which the tile belongs.
		 */
		this.tileIndex = tileIndex;
		/**
		 * True if this tile is currently moved by the user.
		 */
		this._moving = false;
		/**
		 * True if this tile is currently moved by the user.
		 */
		this._resizing = false;

		this.rectangle = null;
		this.syncCustomProperties();
		this.idrefresh(firstClient);
		this.getPosition(true);
	} catch(err) {
		print(err, "in Tile");
	}
}


Tile.prototype.getPosition = function(init) {
	try {
		
		client=this.clients[0];
		
		if (init === true && this.positionConfig[this.id] !== undefined) {
			this.positionConfig[this.id].wid = client.windowId;
			this.positionConfig[this.id].count = 0;
			this.desktops = this.positionConfig[this.id].desktops;
			this.screens = this.positionConfig[this.id].screens;
			this.activities = this.positionConfig[this.id].activities;
			this.tileIndex = this.positionConfig[this.id].tileIndex;
			print(this.id, this.desktops, this.screens, this.activities, this.tileIndex);
			
		} else {
			this.desktops = [];
			this.screens = [];
			
			if (client.desktop == -1) {
				for (j = 0; j < this.parent.parent.desktopCount; j++) {
					if (this.desktops.indexOf(j) == -1) {
						this.desktops.push(j);
					}
				}
			} else {
				this.desktops = [client.desktop-1];
			}
			
			if (client.screen == -1) {
				for (j = 0; j < this.parent.parent.screenCount; j++) {
					if (this.screens.indexOf(j) == -1) {
						this.screens.push(j);
					}
				}
			} else {
				this.screens = [client.screen];
			}
			this.activities = []
			
			if (client.activities.length == 0) {
				for (var i = 0; i < this.parent.parent.activities.length; i++) {
					this.activities.push(i);
				}
			} else {
				for (var i = 0; i < client.activities.length; i++) {
					index = this.parent.parent.activities.indexOf(client.activities[i]);
					if (index > -1) {
						this.activities.push(index);
					}
				}
			}
		}
		this.desktops.sort();
		this.screens.sort();
		this.activities.sort();
		
		this._currentDesktop = this.desktops[0];
		this._currentScreen = this.screens[0];
		this._currentActivity = this.activities[0];
		oldID = this.id;
		this.idrefresh(client);
		newID = this.id;
		
		this.positionSet.emit(oldID, newID);
	} catch(err) {
		print(err, "in Tile.getPosition");
	}
};


Tile.prototype.setPosition = function() {
	if (this.desktops.length > 1) {
		var desktop = -1;
	} else {
		var desktop = this._currentDesktop+1;
	}
	var activities = [];
	for (var i=0; i < this.activities.length; i++) {
		activities.push(this.parent.parent.activities[this.activities[i]]);
	}
	var changed = false
	for(i = 0; i < this.clients.length; i++) {
		if (this.clients[i].desktop != desktop) {
			this.clients[i].desktop = desktop;
			var changed = true
		}
		if (this.clients[i].screen != this.screens[0]) {
			this.clients[i].screen = this.screens[0];
			var changed = true
		}
		if (this.clients[i].activities != activities) {
			this.clients[i].activities = activities;
			var changed = true
		}
		if (changed === true) {
			this.positionChanged.emit(this.clients[i]);
		}
	}
};

/**
 * Sets the geometry of the tile. geometryChanged events caused by this function
 * are suppressed.
 *
 * @param geometry New tile geometry.
 */
Tile.prototype.setGeometry = function(geometry) {
	try {
		if (geometry == null) {
			return;
		}
		if (this.rectangle == null) {
			this.rectangle = Qt.rect(geometry.x,
									 geometry.y,
									 geometry.width,
									 geometry.height);
		} else {
			this.rectangle.x = geometry.x;
			this.rectangle.y = geometry.y;
			this.rectangle.width = geometry.width;
			this.rectangle.height = geometry.height;
		}

		for(i = 0; i < this.clients.length; i++) {
			this.clients[i].tiling_MoveResize = false;
			this.onClientGeometryChanged(this.clients[i]);
		}
	} catch(err) {
		print(err, "in Tile.setGeometry");
	}
};

Tile.prototype.resetGeometry = function() {
	this.setGeometry(this.rectangle);
};

/**
 * Returns the currently active client in the tile.
 */
Tile.prototype.getActiveClient = function() {
	try {
		var active = null;
		this.clients.forEach(function(client) {
			if (client.isCurrentTab) {
				active = client;
			}
		});
		return active;
	} catch(err) {
		print(err, "in Tile.getActiveClient");
	}
};


Tile.prototype.idrefresh = function(client) {
	
	var s = client.caption;
	var id = s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0); 
	id = [client.resourceClass.toString(), id].join('_');
	
	if (this.positionConfig[id] !== undefined && 
			this.positionConfig[id].wid !== undefined &&
			this.positionConfig[id].wid !== client.windowId) {
		if (this.positionConfig[id].count === undefined) {count = 0;} 
		else {count = this.positionConfig[id].count;}
		id = [id, count+1].join('.');
	}
	this.id = id;
}


/**
 * Synchronizes all custom properties (tileIndex, floating between all clients
 * in the tile).
 */
Tile.prototype.syncCustomProperties = function() {
	try {
		var client = this.getActiveClient();
		if (client == null) {
			client = this.clients[0];
		}
		if (client != null) {
			client.tiling_tileIndex = this.tileIndex;
			client.syncTabGroupFor("tiling_tileIndex", true);
			client.syncTabGroupFor("tiling_floating", true);
		}
	} catch(err) {
		print(err, "in Tile.syncCustomProperties");
	}
};


Tile.prototype.onClientCaptionChanged = function(client) {
	try {
		var oldID = this.id;
		this.idrefresh(client);
		var newID = this.id;
		this.captionChanged.emit(oldID, newID);
	} catch(err) {
		print(err, "in Tile.onClientCaptionChanged");
	}
};

Tile.prototype.onClientGeometryChanged = function(client) {
	try {
		if (client == null) {
			return;
		}
		if (client.tiling_tileIndex != this.tileIndex) {
			print("Wrong tile called");
			return;
		}
		// Don't resize when client isn't on current desktop
		// Prevents a couple unnecessary resizes
 		if (client.desktop != workspace.currentDesktop && client.desktop > -1) {
 			return;
 		}
		// These two should never be reached
		if (client.deleted == true) {
			return;
		}
		if (client.managed == false) {
			return;
		}
		if (!client.isCurrentTab) {
			return;
		}
		if (client.move || client.resize) {
			return;
		}
		if (this._moving || this._resizing) {
			this.rectangle.x = client.geometry.x;
			this.rectangle.y = client.geometry.y;
			this.rectangle.width = client.geometry.width;
			this.rectangle.height = client.geometry.height;
			return;
		}
		if (client.resizeable != true) {
			print("Client", client.resourceClass.toString(), "not resizeable");
			return;
		}
		if (client.moveable != true) {
			print("Client", client.resourceClass.toString(), "not moveable");
			return;
		}
		if (client.tiling_resize == true) {
			return;
		}
		if (this.rectangle != null) {
			if (client.geometry.x != this.rectangle.x ||
				client.geometry.y != this.rectangle.y ||
				client.geometry.width != this.rectangle.width ||
				client.geometry.height != this.rectangle.height) {
				client.tiling_resize = true;
				// HACK: Resize the client to null - this makes kwin recreate the pixmap
				client.geometry = null;
			
// 				animate({
// 					window: client,
// 					animations: [
// 						ParallelAnimation {
// 							running: true
// 							NumberAnimation { target: client.geometry; property: "x"; to: this.rectangle.x; duration: 1000 }
// 							NumberAnimation { target: client.geometry; property: "y"; to: this.rectangle.y; duration: 1000 }
// 							NumberAnimation { target: client.geometry; property: "width"; to: this.rectangle.width; duration: 1000 }
// 							NumberAnimation { target: client.geometry; property: "height"; to: this.rectangle.height; duration: 1000 }
// 						}
// 					]
// 				});
				
				client.geometry = Qt.rect(this.rectangle.x,
										  this.rectangle.y,
										  this.rectangle.width,
										  this.rectangle.height);
				print(client.geometry.x, client.geometry.y, client.geometry.width, client.geometry.height);
				client.tiling_resize = false;
			}
		} else {
			print("No rectangle", client.resourceClass.toString(), client.windowId);
		}
	} catch(err) {
		print(err, "in Tile.onClientGeometryChanged");
	}
};

Tile.prototype.onClientDesktopChanged = function(client) {
	try {
		if (!client.isCurrentTab) {
			return;
		}
		var oldDesktops = this.desktops;
		this.getPosition();
		var newDesktops = this.desktops;
		this.desktopChanged.emit(oldDesktops, newDesktops);
		
	} catch(err) {
		print(err, "in Tile.onClientDesktopChanged");
	}
};

Tile.prototype.onClientActivityChanged = function(client) {
	try {
		if (!client.isCurrentTab) {
			return;
		}
		var oldActivities = this.activities;
		this.getPosition();
		var newActivities = this.activities;
		
		this.activityChanged.emit(oldActivities, newActivities);
		
	} catch(err) {
		print(err, "in Tile.onClientActivityChanged");
	}
};

Tile.prototype.onClientScreenChanged = function(client) {
	try {
		if (!client.isCurrentTab) {
			return;
		}
		var oldScreens = this.screens;
		this.getPosition();
		var newScreens = this.screens;
		
		this.screenChanged.emit(oldScreens, newScreens);
		
	} catch(err) {
		print(err, "in Tile.onClientScreenChanged");
	}
};

Tile.prototype.onClientActiveChanged = function(client) {
	try {
		if (!client.isCurrentTab) {
			return;
		}
		uri = callDBus("org.kde.ActivityManager","/SLC","org.kde.ActivityManager.SLC","focussedResourceURI");
		print("focussedResourceURI", uri);
		
	} catch(err) {
		print(err, "in Tile.onClientActiveChanged");
	}
};

Tile.prototype.onClientStartUserMovedResized = function(client) {
};

Tile.prototype.onClientStepUserMovedResized = function(client) {
	try {
		if (client.resize) {
			this.resizingStep.emit();
			// This means it gets animated
			this.resizingEnded.emit();
			this._resizing = true;
			return;
		}
		if (client.move) {
			this.movingStep.emit();
			this.movingEnded.emit();
			this._moving = true;
			return;
		}
	} catch(err) {
		print(err, "in Tile.onClientStepUserMovedResized");
	}
};

Tile.prototype.onClientFinishUserMovedResized = function(client) {
	try {
		if (this._moving) {
			this._moving = false;
			this.movingEnded.emit();
		} else if (this._resizing) {
			this._resizing = false;
			this.resizingEnded.emit();
		}
	} catch(err) {
		print(err, "in Tile.onClientFinishUserMovedResized");
	}
};

Tile.prototype.removeClient = function(client) {
	try {
		this.clients.splice(this.clients.indexOf(client), 1);
	} catch(err) {
		print(err, "in Tile.removeClient");
	}
}

Tile.prototype.addClient = function(client) {
	try {
		if (this.clients.indexOf(client) == -1) {
			this.clients.push(client);
			this.syncCustomProperties();
			this.onClientGeometryChanged(client);
		}
	} catch(err) {
		print(err, "in Tile.addClient");
	}
}

Tile.prototype.onClientMaximizedStateChanged = function(client, h, v) {
	try {
		var screenRect = workspace.clientArea(KWin.PlacementArea, this._currentScreen, this._currentDesktop);
		var newRect = Qt.rect(this.rectangle.x,
							  this.rectangle.y,
							  this.rectangle.width,
							  this.rectangle.height);
		if (h) {
			newRect.x = screenRect.x;
			newRect.width = screenRect.width;
		} else {
			if (this.oldRect != null) {
				newRect.x = this.oldRect.x;
				newRect.width = this.oldRect.width;
			}
		}
		if (v) {
			newRect.y = screenRect.y;
			newRect.height = screenRect.height;
		} else {
			if (this.oldRect != null) {
				newRect.y = this.oldRect.y;
				newRect.height = this.oldRect.height;
			}
		}
		this.oldRect = Qt.rect(this.rectangle.x,
							  this.rectangle.y,
							  this.rectangle.width,
							  this.rectangle.height);
		this.setGeometry(newRect);
	} catch(err) {
		print(err, "in tile.onClientMaximizedStateChanged");
	}
}

Tile.prototype.hasClient = function(client) {
	return (this.clients.indexOf(client) > -1);
}

