import QtQuick 1.1;

import "components";


TilingManager {
	id: tilingmanager
	
	onTileAdded: {
		print('tile added', tile.clients[0].resourceClass.toString());
	}
	
	onTileResized: {
		print('tile resized', tile.clients[0].resourceClass.toString());
	}
	
	onTileRemoved: {
		print('tile removed', tile.clients[0].resourceClass.toString());
	}
	
	onTileActivityChanged: {
		print('tile activity changed', tile.clients[0].resourceClass.toString());
	}
	
	onTileScreenChanged: {
		print('tile screen changed', tile.clients[0].resourceClass.toString());
	}
	
	onTileCaptionChanged: {
		print('tile caption changed', tile.clients[0].resourceClass.toString());
	}
	
	onTilePositionChanged: {
		print('tile position changed', client.resourceClass.toString());
	}
	
	onTilePositionSet: {
		print('tile position set', tile.clients[0].resourceClass.toString());
	}
	
	onTilingActivated: {
		
		showNotice([tiling.layout.name, 'Layout'].join('-'), 
							 ['Desktop', tiling.desktopName, 'on'].join(' '),
							 ['Screen', tiling.screen].join(' ')
							);
	}
	
	onCurrentActivityChanged: {
		print('activity changed', activity);
	}
	
	onCurrentDesktopChanged: {
		print('desktop changed', desktop);
	}
	
	onCurrentScreenChanged: {
		print('screen changed', screen);
	}
}
