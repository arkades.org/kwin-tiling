import QtQuick 1.1;
import org.kde.plasma.core 0.1 as PlasmaCore;
import org.kde.plasma.components 0.1 as Plasma;
import org.kde.qtextracomponents 0.1 as QtExtra;
import org.kde.kwin 0.1;

import "code/logic.js" as Logic;


/**
 * Class which manages all layouts, connects the various signals and handlers
 * and implements all keyboard shortcuts.
 * @class
 */
// Item {
// 	id: self
// 	
// 	
// 	Component.onCompleted {
// 		print('ok');
// 	
// 		/*
// 		self.init = true;
// 		self._sessionOpen = false;
// 		self._activityChange = false;
// 		self.activities = [];
// 		
// 		/**
// 			* Default layout type which is selected for new layouts.
// 			*/
// 		self.defaultLayout = Logic.HalfLayout;
// 
// 		/**
// 			* List of all available layout types.
// 			*/
// 		self.availableLayouts = [
// 			Logic.HalfLayout,
// 			Logic.BladeLayout,
// 			Logic.StackLayout,
// 			Logic.ValfLayout,
// 			Logic.SpiralLayout/*,
// 			Logic.ZigZagLayout,
// 			Logic.ColumnLayout,
// 			Logic.RowLayout,
// 			Logic.GridLayout,
// 			Logic.MaximizedLayout,
// 			Logic.FloatingLayout*/
// 		];
// 		
// 		
// 		/**
// 			* Array containing a list of layouts for every desktop. Each of the lists
// 			* has one element per screen and activity.
// 			*/
// 		self.layouts = [];
// 		/**
// 			* List of all tiles in the system.
// 			*/
// 		self.tiles = new Logic.TileList(self);
// 			/**
// 			* True if a user moving operation is in progress.
// 			*/
// 		self._moving = false;
// 		/**
// 			* The screen where the current window move operation started.
// 			*/
// 		self._movingStartScreen = 0;
// 		/**
// 		* Whether tiling is active on all desktops
// 		* This is overridden by per-desktop settings
// 		*/
// 		self.userActive = readConfig("userActive", true);
// 		
// 		/**
// 		* Read Layout and Positions Configuration Values.
// 		*/
// 		self._readLayoutConf()
// 		self._readPositionsConf()
// 
// 		// Connect the tile list signals so that new tiles are added to the layouts
// 		self.tiles.tileAdded.connect(function(tile) {
// 				self._onTileAdded(tile);
// 		});
// 		self.tiles.tileRemoved.connect(function(tile) {
// 				self._onTileRemoved(tile);
// 		});
// 		
// 		registerShortcut("Tiling Tests",
// 										"Tiling Tests",
// 										"Meta+T",
// 										function() {
// 											print('start');
// 											self._start()
// 										});
// 		
// 		workspace.sessionOpened.connect(function() {
// 			self._start();
// 		});
// 		
// 	}
// 
// 
// 	function _start() {
// 		try {
// 			self._sessionOpen = true;
// 			/**
// 			* Activities in the system.
// 			*/
// 			self.activities = workspace.activities;
// 			/**
// 			* Current Activity in the system.
// 			*/
// 			self._currentActivity = workspace.currentActivity;
// 			/**
// 			* Number of activities in the system.
// 			*/
// 			self.activityCount = self.activities.length;
// 			/**
// 			* Number of desktops in the system.
// 			*/
// 			self.desktopCount = workspace.desktopGridWidth
// 			* workspace.desktopGridHeight;
// 			/**
// 			* Number of screens in the system.
// 			*/
// 			self.screenCount = workspace.numScreens;
// 			/**
// 			* Current screen, needed to be able to track screen changes.
// 			*/
// 			self._currentScreen = workspace.activeScreen;
// 			/**
// 			* Current desktop, needed to be able to track screen changes.
// 			*/
// 			self._currentDesktop = workspace.currentDesktop - 1;
// 			
// 			
// 			self._registerCallbacks();
// 			self._registerShortcuts();
// 			
// 			// Create the various layouts, one for every desktop
// 			for (var i = 0; i < self.desktopCount; i++) {
// 				self._createDefaultLayouts(i);
// 			}
// 			
// 			var existingClients = workspace.clientList();
// 			for (var i=0; i < existingClients.length; i++) {
// 				self.tiles.addClient(existingClients[i]);
// 			}
// 			self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 			self._init = false;
// 			self.onCompleted.emit();
// 		} catch (err) {
// 			print(err, 'in TilingManager.start');
// 		}
// 	}
// 
// 
// 	function _registerCallbacks() {
// 		// Register global callbacks
// 		
// 		workspace.numberDesktopsChanged.connect(function() {
// 			self._onNumberDesktopsChanged();
// 		});
// 		workspace.numberScreensChanged.connect(function() {
// 			self._onNumberScreensChanged();
// 		});
// 		workspace.activitiesChanged.connect(function(activity) {
// 			self._onNumberActivitiesChanged(activity);
// 		});
// 		workspace.screenResized.connect(function(screen) {
// 			try {
// 				self._onScreenResized(screen);
// 			} catch(err) {
// 				print(err);
// 			}
// 		});
// 		workspace.currentDesktopChanged.connect(function() {
// 			self._onCurrentDesktopChanged();
// 		});
// 		workspace.currentActivityChanged.connect(function(activity) {
// 			self._onCurrentActivityChanged(activity);
// 		});
// 	}
// 
// 
// 	function _registerShortcuts() {
// 		// Register keyboard shortcuts
// 		registerShortcut("Next Tiling Layout",
// 										"Next Tiling Layout",
// 										"Meta+PgDown",
// 										function() {
// 											var currentLayout = self._getCurrentLayoutType();
// 											var nextIndex = (currentLayout.index + 1) % self.availableLayouts.length;
// 											self._switchLayout(workspace.currentDesktop - 1,
// 																				workspace.activeScreen,
// 															self._currentActivity,
// 															nextIndex);
// 										});
// 		registerShortcut("Previous Tiling Layout",
// 										"Previous Tiling Layout",
// 										"Meta+PgUp",
// 										function() {
// 											var currentLayout = self._getCurrentLayoutType();
// 											var nextIndex = currentLayout.index - 1;
// 											if (nextIndex < 0) {
// 												nextIndex += self.availableLayouts.length;
// 											}
// 											self._switchLayout(workspace.currentDesktop - 1,
// 																				workspace.activeScreen,
// 															self._currentActivity,
// 															nextIndex);
// 										});
// 		registerShortcut("Toggle Floating",
// 										"Toggle Floating",
// 										"Meta+F",
// 										function() {
// 											var client = workspace.activeClient;
// 											if (client == null) {
// 												print("No active client");
// 												return;
// 											}
// 											client.tiling_floating = !client.tiling_floating;
// 											if (client.tiling_floating == true) {
// 												self.tiles._onClientRemoved(client);
// 											} else {
// 												self.tiles.addClient(client);
// 											}
// 										});
// 		registerShortcut("Toggle Border",
// 										"Toggle Border",
// 										"Meta+U",
// 										function() {
// 											var client = workspace.activeClient;
// 											if (client == null) {
// 												print("No active client");
// 												return;
// 											}
// 											client.noBorder = ! client.noBorder;
// 											if (client.noBorder == false) {
// 												client.tiling_floating = true;
// 												self.tiles._onClientRemoved(client);
// 											} else {
// 												client.tiling_floating = false;
// 												self.tiles.addClient(client);
// 											}
// 										});
// 		registerShortcut("Toggle Border for all",
// 										"Toggle Border for all",
// 										"Meta+Shift+U",
// 										function() {
// 											self.tiles.toggleNoBorder()
// 										});
// 		registerShortcut("Switch Focus Left",
// 										"Switch Focus Left",
// 										"Meta+H",
// 										function() {
// 											self._switchFocus(Direction.Left);
// 										});
// 		registerShortcut("Switch Focus Right",
// 										"Switch Focus Right",
// 										"Meta+L",
// 										function() {
// 											self._switchFocus(Direction.Right);
// 										});
// 		registerShortcut("Switch Focus Up",
// 										"Switch Focus Up",
// 										"Meta+K",
// 										function() {
// 											self._switchFocus(Direction.Up);
// 										});
// 		registerShortcut("Switch Focus Down",
// 										"Switch Focus Down",
// 										"Meta+J",
// 										function() {
// 											self._switchFocus(Direction.Down);
// 										});
// 		registerShortcut("Move Window Left",
// 										"Move Window Left",
// 										"Meta+Shift+H",
// 										function() {
// 											self._moveTile(Direction.Left);
// 										});
// 		registerShortcut("Move Window Right",
// 										"Move Window Right",
// 										"Meta+Shift+L",
// 										function() {
// 											self._moveTile(Direction.Right);
// 										});
// 		registerShortcut("Move Window Up",
// 										"Move Window Up",
// 										"Meta+Shift+K",
// 										function() {
// 											self._moveTile(Direction.Up);
// 										});
// 		registerShortcut("Move Window Down",
// 										"Move Window Down",
// 										"Meta+Shift+J",
// 										function() {
// 											self._moveTile(Direction.Down);
// 										});
// 		registerShortcut("Toggle Tiling",
// 										"Toggle Tiling",
// 										"Meta+Shift+f11",
// 										function() {
// 											self.layouts[workspace.currentDesktop - 1][workspace.activeScreen][self._currentActivity].toggleUserActive();
// 										});
// 		registerShortcut("Swap Window With Master",
// 										"Swap Window With Master",
// 										"Meta+Shift+M",
// 										function() {
// 											try {
// 												var layout = self.layouts[workspace.currentDesktop - 1][workspace.activeScreen][self._currentActivity];
// 												if (layout != null) {
// 													var client = workspace.activeClient;
// 													if (client != null) {
// 														var tile = layout.getTile(client.x, client.y);
// 													}
// 												}
// 												if (tile != null) {
// 													layout.swapTiles(tile, layout.tiles[0]);
// 												}
// 											} catch(err) {
// 												print(err, "in swap-window-with-master");
// 											}
// 										});
// 		registerShortcut("Resize Active Window To The Left",
// 										"Resize Active Window To The Left",
// 										"Meta+Alt+H",
// 										function() {
// 											try {
// 												var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
// 												// Qt.rect automatically clamps negative values to zero
// 												geom = Qt.rect(tile.rectangle.x - 10,
// 																			tile.rectangle.y,
// 														tile.rectangle.width - 10,
// 														tile.rectangle.height);
// 												if (geom.x < 0) {
// 													geom.x = 0;
// 												}
// 												self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
// 											} catch(err) {
// 												print(err, "in resize-window-to-the-left");
// 											}
// 										});
// 		registerShortcut("Resize Active Window To The Right",
// 										"Resize Active Window To The Right",
// 										"Meta+Alt+L",
// 										function() {
// 											try {
// 												var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
// 												// Qt.rect automatically clamps negative values to zero
// 												geom = Qt.rect(tile.rectangle.x,
// 																			tile.rectangle.y,
// 														tile.rectangle.width + 10,
// 														tile.rectangle.height);
// 												self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
// 											} catch(err) {
// 												print(err, "in resize-window-to-the-left");
// 											}
// 										});
// 		registerShortcut("Resize Active Window To The Top",
// 										"Resize Active Window To The Top",
// 										"Meta+Alt+K",
// 										function() {
// 											try {
// 												var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
// 												// Qt.rect automatically clamps negative values to zero
// 												geom = Qt.rect(tile.rectangle.x,
// 																			tile.rectangle.y - 10,
// 														tile.rectangle.width,
// 														tile.rectangle.height + 10);
// 												self.layouts[tile._currentDesktop - 1][tile._currentScreen][tile._currentActivity].resizeMaster(geom);
// 											} catch(err) {
// 												print(err, "in resize-window-to-the-left");
// 											}
// 										});
// 		registerShortcut("Resize Active Window To The Bottom",
// 										"Resize Active Window To The Bottom",
// 										"Meta+Alt+J",
// 										function() {
// 											try {
// 												var tile = self._getMaster(self._currentScreen, self._currentDesktop, self._currentActivity);
// 												// Qt.rect automatically clamps negative values to zero
// 												geom = Qt.rect(tile.rectangle.x,
// 																			tile.rectangle.y,
// 														tile.rectangle.width,
// 														tile.rectangle.height - 10);
// 												self.layouts[tile._currentDesktop - 1][tile._currentScreen].resizeMaster(geom);
// 											} catch(err) {
// 												print(err, "in resize-window-to-the-left");
// 											}
// 										});
// 		registerUserActionsMenu(function(client) {
// 			return {
// 				text : "Toggle floating",
// 				triggered: function () {
// 					client.tiling_floating = ! client.tiling_floating;
// 					if (client.tiling_floating == true) {
// 						self.tiles._onClientRemoved(client);
// 					} else {
// 						self.tiles.addClient(client);
// 					}
// 				}
// 			};
// 		});
// 	}
// 
// 
// 	function _createDefaultLayouts(desktop) {
// 		try {
// 			var screenLayouts = [];
// 			var activityLayouts = [];
// 			var i = desktop;
// 			
// 			for (var j = 0; j < self.screenCount; j++) {
// 				for (var k = 0; k < self.activityCount; k++) {
// 					if (typeof(self.layoutConfig[i]) !== 'undefined' && 
// 							typeof(self.layoutConfig[i][j]) !== 'undefined' && 
// 							typeof(self.layoutConfig[i][j][k]) !== 'undefined') {
// 						var userConfig = true;
// 						var layout = self.layoutConfig[i][j][k].layout;
// 						var tiling = self.layoutConfig[i][j][k].tiling;
// 					} else {
// 						var layout = self.defaultLayout;
// 						var tiling = false;
// 						var userConfig = false;
// 					}
// 					
// 					activityLayouts[k] = new Logic.Tiling(layout, i, j, k);
// 					activityLayouts[k].tilingActivated.connect(function(tiling) {
// 						self._onTilingActivated(tiling);
// 					});
// 			// Either the default is to tile and the desktop hasn't been configured,
// 			// or the desktop has been set to tile (in which case the default is irrelevant)
// 					activityLayouts[k].userActive = (self.userActive == true && userConfig == false) || (tiling == true);
// 					print('TilingManager._createDefaultLayouts', i, j, k);
// 				}
// 				screenLayouts[j] = activityLayouts
// 			}
// 			self.layouts[i] = screenLayouts;
// 		} catch (err) {
// 			print(err, 'in TilingManager._createDefaultLayouts');
// 		}
// 	}
// 
// 	function _getCurrentLayoutType() {
// 		try {
// 			var currentLayout = self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity];
// 			return currentLayout.layoutType;
// 		} catch (err) {
// 			print(err, 'in TilingManager._getCurrentLayoutType');
// 		}
// 	}
// 
// 	function _onTileAdded(tile) {
// 			// Add tile callbacks which are needed to move the tile between different
// 			// screens/desktops
// 		try {
// 			
// 			tile.captionChanged.connect(function(oldID, newID) {
// 				self._onTileCaptionChanged(tile, oldID, newID);
// 			});
// 			tile.screenChanged.connect(function(oldScreen, newScreen) {
// 				self._onTileScreenChanged(tile, oldScreen, newScreen);
// 			});
// 			tile.desktopChanged.connect(function(oldDesktop, newDesktop) {
// 				self._onTileDesktopChanged(tile, oldDesktop, newDesktop);
// 			});
// 			tile.activityChanged.connect(function(oldActivity, newActivity) {
// 				self._onTileActivityChanged(tile, oldActivity, newActivity);
// 			});
// 			tile.positionChanged.connect(function(oldID, newID) {
// 				self._onTilePositionChanged(tile, oldID, newID);
// 			});
// 			tile.movingStarted.connect(function() {
// 				self._onTileMovingStarted(tile);
// 			});
// 			tile.movingEnded.connect(function() {
// 				self._onTileMovingEnded(tile);
// 			});
// 			tile.resizingEnded.connect(function() {
// 				self._onTileResized(tile);
// 			});
// 			// Add the tile to the layouts
// 			for (var i = 0; i < tile.desktops.length; i++) {
// 				for (var j = 0; j < tile.screens.length; j++) {
// 					for (var k = 0; k < tile.activities.length; k++) {
// 						var tileLayouts = self._getLayouts(tile.desktops[i], tile.screens[j], tile.activities[k]);
// 						tileLayouts.forEach(function(layout) {
// 							layout.addTile(tile);
// 						});
// 					}
// 				}
// 			}
// 			
// 			if (tile.desktops.indexOf(self._currentDesktop) == -1) {
// 				self._currentDesktop = tile.desktops[0];
// 			}
// 			if (tile.screens.indexOf(self._currentScreen) == -1) {
// 				self._currentScreen = tile.screens[0];
// 			}
// 			if (tile.activities.indexOf(self._currentActivity) == -1) {
// 				self._currentActivity = tile.activities[0];
// 			}
// 			if (self.init == false) {
// 				self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._onTileAdded');
// 		}
// 	}
// 
// 	function _onTileResized(tile) {
// 		try {
// 			var tileLayouts = self._getLayouts(self._currentDesktop, self._currentScreen, self._currentActivity);
// 			tileLayouts.forEach(function(layout) {
// 				layout.resizeTile(tile);
// 			});
// 		} catch (err) {
// 			print(err, 'in TilingManager._onTileResized');
// 		}
// 	}
// 
// 	function _getMaster(screen, desktop, activity) {
// 		try {
// 			return self.layouts[desktop][screen][activity].getMaster();
// 		} catch(err) {
// 			print(err, "in TilingManager._getMaster");
// 		}
// 	}
// 
// 	function _onTileRemoved(tile) {
// 		try {
// 			var tileLayouts = self._getLayouts(tile.desktops, tile.screens, tile.activities);
// 			tileLayouts.forEach(function(layout) {
// 				layout.removeTile(tile);
// 			});
// 		} catch(err) {
// 			print(err, "in TilingManager._onTileRemoved");
// 		}
// 	}
// 
// 	function _onNumberDesktopsChanged() {
// 		try {
// 			var newDesktopCount =
// 				workspace.desktopGridWidth * workspace.desktopGridHeight;
// 			var onAllDesktops = tiles.tiles.filter(function(tile) {
// 				return (tile.desktop == -1 || tile.desktops.indexOf(newDesktopCount-1) > -1);
// 			});
// 			// Remove tiles from desktops which do not exist any more (we only have to
// 			// care about tiles shown on all desktops as all others have been moved away
// 			// from the desktops by kwin before)
// 			onAllDesktops.forEach(function(tile) {
// 				for (var i = tile.desktops.indexOf(newDesktopCount); i < tile.desktops.length; i++) {
// 					for (var j = 0; j < tile.screens.length; j++) {
// 						for (var k = 0; k < tile.activities.length; k++) {
// 							self.layouts[tile.desktops[i]][tile.screens[j]][tile.activities[k]].removeTile(tile);
// 						}
// 					}
// 				}
// 			});
// 			
// 			// Add new desktops
// 			for (var i = self.desktopCount; i < newDesktopCount; i++) {
// 				self._createDefaultLayouts(i);
// 				onAllDesktops.forEach(function(tile) {
// 					for (var j = 0; j < tile.screens.length; j++) {
// 						for (var k = 0; k < tile.activities.length; k++) {
// 							self.layouts[i][tile.screens[j]][tile.activities[k]].addTile(tile);
// 						}
// 					}	
// 				});
// 			}
// 			// Remove deleted desktops
// 			if (self.desktopCount > newDesktopCount) {
// 				self.layouts.length = newDesktopCount;
// 			}
// 			self.desktopCount = newDesktopCount;
// 			self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 		} catch (err) {
// 			print(err, 'in TilingManager._onNumberDesktopsChanged');
// 		}
// 	}
// 
// 	function _onNumberScreensChanged() {
// 		// Add new screens
// 		try {
// 			var newSceenCount = workspace.numScreens;
// 			var onAllDesktops = tiles.tiles.filter(function(tile) {
// 				return (tile.screen == -1 || tile.screens.indexOf(newScreenCount-1) > -1);
// 			});
// 			
// 			
// 			onAllDesktops.forEach(function(tile) {
// 				for (var i = tile.screens.indexOf(newScreenCount-1); i < tile.screens.length; i++) {
// 					for (var j = 0; j < tile.desktops.length; j++) {
// 						for (var k = 0; k < tile.activities.length; k++) {
// 							self.layouts[tile.desktops[j]][tile.screens[i]][tile.activities[k]].removeTile(tile);
// 						}
// 					}
// 				}
// 			});
// 			
// 			// Add new screens
// 			if (self.screenCount < workspace.numScreens) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					for (var j = self.screenCount; j < workspace.numScreens; j++) {
// 						for (var k = self.activityCount; k < self.activities.length; k++) {
// 							self.layouts[i][j][k] = new Logic.Tiling(self.defaultLayout, i, j, k);
// 						}
// 					}
// 				}
// 			}
// 			
// 			for (var i = self.screenCount; i < newScreenCount; i++) {
// 				onAllDesktops.forEach(function(tile) {
// 					for (var j = 0; j < tile.desktops.length; j++) {
// 						for (var k = 0; k < tile.activities.length; k++) {
// 							self.layouts[tile.desktops[j]][tile.screens[i]][tile.activities[k]].addTile(tile);
// 						}
// 					}	
// 				});
// 			}
// 			
// 			// Remove deleted screens
// 			if (self.screenCount > workspace.numScreens) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					self.layouts[i].length = workspace.numScreens;
// 				}
// 			}
// 			// Remove deleted activities
// 			if (self.activityCount > self.activities.length) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					for (var j = 0; j < self.screenCount; j++) {
// 						self.layouts[i][j].length = self.activities.length;
// 					}
// 				}
// 			}
// 			self.activityCount = self.activities.length;
// 			self.screenCount = workspace.numScreens;
// 			self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 		} catch (err) {
// 			print(err, 'in TilingManager._onNumberScreensChanged');
// 		}
// 	}
// 
// 	function _onNumberActivitiesChanged(activity) {
// 		// Add new activities
// 		try {
// 			self.activities = self.dataengine.sources.slice(0,activities.sources.length-1);
// 			newActivityCount = self.activities.length;
// 			
// 			var onAllDesktops = tiles.tiles.filter(function(tile) {
// 				return (tile.activities.indexOf(newActivityCount-1) > -1);
// 			});
// 			
// 			if (self.activityCount < self.activities.length) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					for (var j = self.screenCount; j < workspace.numScreens; j++) {
// 						for (var k = self.activityCount; k < self.activities.length; k++) {
// 							self.layouts[i][j][k] = new Logic.Tiling(self.defaultLayout, i, j, k);
// 						}
// 					}
// 				}
// 			}
// 			
// 			// Remove deleted screens
// 			if (self.screenCount > workspace.numScreens) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					self.layouts[i].length = workspace.numScreens;
// 				}
// 			}
// 			// Remove deleted activities
// 			if (self.activityCount > self.activities.length) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					for (var j = 0; j < self.screenCount; j++) {
// 						self.layouts[i][j].length = self.activities.length - 1;
// 					}
// 				}
// 			}
// 			self._currentActivity = self.activities.indexOf(activity);
// 			self.activityCount = self.activities.length;
// 			self.screenCount = workspace.numScreens;
// 			self._onCurrentActivityChanged();
// 		} catch (err) {
// 			print(err, 'in TilingManager._onNumberActivitiesChanged');
// 		}
// 	}
// 
// 	function _onScreenResized(screen) {
// 		try {
// 			if (screen < self.screenCount) {
// 				for (var i = 0; i < self.desktopCount; i++) {
// 					for (var k = 0; i < self.activityCount; k++) {
// 						self.layouts[i][screen][k].activate();
// 					}
// 				}
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._onScreenResized');
// 		}
// 	}
// 
// 	function _onTileActivityChanged(tile, oldActivity, newActivity) {
// 		// If a tile is moved by the user, screen changes are handled in the move
// 		// callbacks below
// 		try {
// 			if (self._moving) {
// 				return;
// 			}
// 			var oldLayouts = self._getLayouts(tile.desktops, tile.screens, oldActivity);
// 			var newLayouts = self._getLayouts(tile.desktops, tile.screens, newActivity);
// 			self._changeTileLayouts(tile, oldLayouts, newLayouts);
// 		} catch(err) {
// 			print(err, "in TilingManager._onTileDesktopChanged");
// 		}
// 		
// 	}
// 
// 	function _onTileScreenChanged(tile, oldScreen, newScreen) {
// 			// If a tile is moved by the user, screen changes are handled in the move
// 			// callbacks below
// 		try {
// 			if (self._moving) {
// 				return;
// 			}
// 			var oldLayouts = self._getLayouts(tile.desktops, oldScreen, tile.activities);
// 			var newLayouts = self._getLayouts(tile.desktops, newScreen, tile.activities);
// 			self._changeTileLayouts(tile, oldLayouts, newLayouts);
// 		} catch (err) {
// 			print(err, 'in TilingManager._onTileScreenChanged');
// 		}
// 	}
// 
// 	function _onTileDesktopChanged(tile, oldDesktop, newDesktop) {
// 		try {
// 			var oldLayouts = self._getLayouts(oldDesktop, tile.screens, tile.activities);
// 			var newLayouts = self._getLayouts(newDesktop, tile.screens, tile.activities);
// 			self._changeTileLayouts(tile, oldLayouts, newLayouts);
// 		} catch(err) {
// 			print(err, "in TilingManager._onTileDesktopChanged");
// 		}
// 	}
// 	
// 	function _onTileCaptionChanged(tile, oldID, newID) {
// 		try {
// 			self._changeTileIdConfig(tile, oldID, newID);
// 		} catch(err) {
// 			print(err, "in TilingManager._onTileCaptionChanged");
// 		}
// 	}
// 	
// 	function _onTilePositionChanged(tile, oldID, newID) {
// 		try {
// 			self._changeTilePosConfig(tile, oldID, newID);
// 		} catch(err) {
// 			print(err, "in TilingManager._onTilePositionChanged");
// 		}
// 	}
// 
// 	function _onTileMovingStarted(tile) {
// 		// NOTE: This supports only one moving window, breaks with multitouch input
// 		try {
// 			self._moving = true;
// 			self._movingStartScreen = tile.clients[0].screen;
// 		} catch (err) {
// 			print(err, 'in TilingManager._onTileMovingStarted');
// 		}
// 	}
// 
// 	function _onTileMovingEnded(tile) {
// 		try {
// 			var client = tile.clients[0];
// 			self._moving = false;
// 			var movingEndScreen = client.screen;
// 			var windowRect = client.geometry;
// 			if (client.tiling_tileIndex >= 0) {
// 				if (self._movingStartScreen != movingEndScreen) {
// 					// Transfer the tile from one layout to another layout
// 					var startLayout = self.layouts[self._currentDesktop][self._movingStartScreen][self._currentActivity];
// 					var endLayout = self.layouts[self._currentDesktop][client.screen][self._currentActivity];
// 					startLayout.removeTile(tile);
// 					endLayout.addTile(tile, windowRect.x + windowRect.width / 2,
// 										windowRect.y + windowRect.height / 2);
// 				} else {
// 					// Transfer the tile to a different location in the same layout
// 					var layout = self.layouts[self._currentDesktop][client.screen][self._currentActivity];
// 					var targetTile = layout.getTile(windowRect.x + windowRect.width / 2,
// 													windowRect.y + windowRect.height / 2);
// 					// In case no tile is found (e.g. middle of the window is offscreen), move the client back
// 					if (targetTile == null) {
// 						targetTile = tile;
// 					}
// 					// swapTiles() works correctly even if tile == targetTile
// 					layout.swapTiles(tile, targetTile);
// 				}
// 			}
// 		} catch(err) {
// 			print(err, "in TilingManager._onTileMovingEnded");
// 		}
// 	}
// 
// 
// 	function _onTilingActivated(tiling) {
// 		try {
// 			self.tiles._updateAllTilePositions();
// 			
// 			self._currentScreen = workspace.activeScreen;
// 			self._currentActivity = callDBus("org.kde.ActivityManager","/ActivityManager/Activities","org.kde.ActivityManager.Activities","CurrentActivity");
// 			self._currentDesktop = workspace.currentDesktop-1;
// 			if (self._currentActivity != tiling.activity) {
// 				self._currentActivity = tiling.activity;
// 				callDBus("org.kde.ActivityManager","/ActivityManager/Activities","org.kde.ActivityManager.Activities","SetCurrentActivity", self.activities[self._currentActivity]);
// 				self.activityChange = false;
// 			}
// 			if (self._currentDesktop != tiling.desktop) {
// 				self._currentDesktop = tiling.desktop;
// 				workspace.currentDesktop = self._currentDesktop+1;
// 				self.desktopChange = false;
// 			}
// 			if (self._currentScreen != tiling.screen) {
// 				self._currentScreen = tiling.screen;
// 				workspace.activeScreen =  tiling.screen;
// 			}
// 			
// 			self._writePositionsConf();
// 		} catch (err) {
// 			print(err, 'in TilingManager._switchActivity()');
// 		}
// 	}
// 
// 
// 	function _changeTileIdConfig(tile, oldID, newID) {
// 		try {
// 			if (self.tiles.positionConfig[oldID] !== undefined) {
// 				oldConf = self.tiles.positionConfig[oldID];
// 				var tileposition = {};
// 				tileposition.count = oldConf.count;
// 				tileposition.wid = oldConf.wid;
// 				tileposition.desktops = oldConf.desktops;
// 				tileposition.activities = oldConf.activities;
// 				tileposition.screens = oldConf.screens;
// 				tileposition.tileindex = oldConf.tileIndex;
// 				self.tiles.positionConfig[newID] = tileposition;
// 				delete self.tiles.positionConfig[oldID];
// 			} else {
// 				var tileposition = {};
// 				tileposition.count = 0;
// 				tileposition.wid = tile.clients[0].windowId;
// 				tileposition.desktops = tile.desktops;
// 				tileposition.activities = tile.activities;
// 				tileposition.screens = tile.screens;
// 				tileposition.tileindex = tile.tileIndex;
// 				self.tiles.positionConfig[newID] = tileposition;
// 			}
// 			
// 			self._writePositionsConf();
// 		} catch (err) {
// 			print(err, 'in TilingManager._changeTileConfig');
// 		}
// 	}
// 
// 	function _changeTilePosConfig(tile, oldID, newID) {
// 		try {
// 			var tileposition = {};
// 			tileposition.count = 0;
// 			tileposition.wid = tile.clients[0].windowId;
// 			tileposition.desktops = tile.desktops;
// 			tileposition.activities = tile.activities;
// 			tileposition.screens = tile.screens;
// 			tileposition.tileindex = tile.tileIndex;
// 			self.tiles.positionConfig[newID] = tileposition;
// 			
// 			if (self.tiles.positionConfig[oldID] !== undefined) {
// 				delete self.tiles.positionConfig[oldID];
// 			} 
// 		} catch (err) {
// 			print(err, 'in TilingManager._changeTileConfig');
// 		}
// 	}
// 
// 	function _changeTileLayouts(tile, oldLayouts, newLayouts) {
// 		try {
// 			oldLayouts.forEach(function(layout) {
// 				layout.removeTile(tile);
// 			});
// 			newLayouts.forEach(function(layout) {
// 				layout.addTile(tile);
// 			});
// 		} catch(err) {
// 			print(err, "in TilingManager._changeTileLayouts");
// 		}
// 	}
// 
// 	function _onCurrentDesktopChanged() {
// 		try {
// 			if (self._currentDesktop == workspace.currentDesktop-1) {
// 				return;
// 			}
// 			if (TilingManager._activityChange == false) {
// 				if (self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 					self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].deactivate();
// 				}
// 				self._currentDesktop = workspace.currentDesktop - 1;
// 				if (! self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 					self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 				}
// 			} else {
// 				workspace.currentDesktop = self._currentDesktop+1;
// 				TilingManager._activityChange = false;
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._onCurrentDesktopChanged');
// 		}
// 		
// 	}
// 
// 	function _onCurrentScreenChanged() {
// 		try {
// 			if (self._currentScreen == workspace.activeScreen) {
// 				return;
// 			}
// 			// TODO: This is wrong, we need to activate *all* visible layouts
// 			if (self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 				self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].deactivate();
// 			}
// 			self._currentScreen = workspace.activeScreen;
// 			if (! self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 				self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._onCurrentScreenChanged');
// 		}
// 	}
// 
// 	function _onCurrentActivityChanged(activity) {
// 		try {
// 			if (self._currentActivity == self.activities.indexOf(activity)) {
// 				return;
// 			}
// 			
// 			TilingManager._activityChange = true;
// 			if (self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 				self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].deactivate();
// 			}
// 			self._currentActivity = self.activities.indexOf(activity);
// 			if (! self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].active) {
// 				self.layouts[self._currentDesktop][self._currentScreen][self._currentActivity].activate();
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._onCurrentActivityChanged');
// 		}
// 		
// 	}
// 
// 	function _readLayoutConf() {
// 		// Read layout configuration
// 		// Format: desktop:layoutname[,...]
// 		// Negative desktop number deactivates tiling
// 		
// 		try {
// 			for (var i = 0; i < self.availableLayouts.length; i++) {
// 				self.availableLayouts[i].index = i;
// 			}
// 			
// 			
// 			self.layoutConfig = [];
// 			var lC = String(readConfig("layouts", "")).replace(/ /g,"").split(",");
// 			for (var i = 0; i < lC.length; i++) {
// 				var layout = lC[i].split(":");
// 				try {
// 					var desktop = parseInt(layout[0]);
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var screen = parseInt(layout[1]);
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var activity = parseInt(layout[2]);
// 				} catch (err) {
// 					continue;
// 				}
// 				var l = self.defaultLayout;
// 				for (var j = 0; j < self.availableLayouts.length; j++) {
// 					if (self.availableLayouts[j].name == layout[3]) {
// 						l = self.availableLayouts[j];
// 					}
// 				}
// 				if (desktop < 0) {
// 					var tiling = false;
// 					desktop = desktop * -1;
// 				} else {
// 					var tiling = true;
// 				}
// 				if (desktop == 0) {
// 					self.defaultLayout = l;
// 				}
// 				// Subtract 1 because the config is in user-indexed format
// 				desktop = desktop - 1;
// 				var desktoplayout = {};
// 				
// 				desktoplayout.desktop = desktop;
// 				desktoplayout.activity = activity;
// 				desktoplayout.screen = screen;
// 				desktoplayout.layout = l;
// 				desktoplayout.tiling = tiling;
// 				if (Object.prototype.toString.call( self.layoutConfig[desktop] ) !== '[object Array]') {
// 					self.layoutConfig[desktop] = [];
// 				}
// 				if (Object.prototype.toString.call( self.layoutConfig[desktop][screen] ) !== '[object Array]') {
// 					self.layoutConfig[desktop][screen] = [];
// 				}
// 				
// 				self.layoutConfig[desktop][screen][activity] = desktoplayout;
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._readLayoutConf');
// 		}
// 	}
// 
// 	function _readPositionsConf() {
// 		try {
// 			var pC = String(readConfig("positions", "")).replace(/ /g,"").split(",");
// 			for (var i = 0; i < pC.length; i++) {
// 				var position = pC[i].split(";");
// 				try {
// 					var id = position[0];
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var desktops = position[1].split(':');
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var screens = position[2].split(':');
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var activities = position[3].split(':');
// 				} catch (err) {
// 					continue;
// 				}
// 				try {
// 					var tileIndex = position[4];
// 				} catch (err) {
// 					continue;
// 				}
// 				
// 				var tileposition = {};
// 				tileposition.desktops = desktops;
// 				tileposition.activities = activities;
// 				tileposition.screens = screens;
// 				tileposition.tileIndex = tileIndex;
// 				self.tiles.positionConfig[id] = tileposition;
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._readPositionsConf');
// 		}
// 	}
// 
// 	function _writeLayoutConf() {
// 		try {
// 			var write = [];
// 			for (var i = 1; i <= self.desktopCount; i++) {
// 				for (var j = 0; j < self.screenCount; j++) {
// 					for (var k = 0; k < self.activityCount; k++) {
// 						entry = [i, j, k, [self.layouts[i-1][j][k].layout.name, 'Layout'].join('')].join(':');
// 						write.push(entry);
// 					}
// 				}
// 			}
// 			writeConfig('layouts', write.join());
// 		} catch (err) {
// 			print(err, 'in TilingManager._writeLayoutConf');
// 		}
// 	}
// 
// 	function _writePositionsConf() {
// 		try {
// 			var write = [];
// 			for (ids in self.tiles.positionConfig) {
// 				pC = self.tiles.positionConfig[ids];
// 				write.push([ids, pC.desktops.join(':'), pC.screens.join(':'), pC.activities.join(':'), pC.tileIndex].join(';'))
// 			}
// 			
// 			writeConfig('positions',""); // write.join());
// 		} catch (err) {
// 			print(err, 'in TilingManager._writePositionsConf');
// 		}
// 	}
// 
// 	function _switchLayout(desktop, screen, activity, layoutIndex) {
// 		// TODO: Show the layout switcher dialog
// 		try {
// 			print('TilingManager._switchLayout', desktop, screen, activity);
// 			var layoutType = self.availableLayouts[layoutIndex];
// 			self.layouts[desktop][screen][activity].setLayoutType(layoutType);
// 			self._writeLayoutConf();
// 		} catch (err) {
// 			print(err, 'in TilingManager._switchLayout');
// 		}
// 	}
// 
// 
// 
// 	function _switchFocus(direction) {
// 		try {
// 			var client = workspace.activeClient;
// 			if (client == null) {
// 				return;
// 			}
// 			var activeTile = self.tiles.getTile(client);
// 			if (activeTile == null) {
// 				return;
// 			}
// 			var layout = self.layouts[client.desktop - 1][self._currentScreen][self._currentActivity];
// 			var nextTile = layout.getAdjacentTile(activeTile, direction, false);
// 			if (nextTile != null && nextTile != activeTile) {
// 				workspace.activeClient = nextTile.getActiveClient();
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._switchFocus');
// 		}
// 	}
// 
// 	function _moveTile(direction) {
// 		try {
// 			var client = workspace.activeClient;
// 			if (client == null) {
// 				return;
// 			}
// 			var activeTile = self.tiles.getTile(client);
// 			if (activeTile == null) {
// 				print("Tile is floating");
// 				return;
// 			}
// 			var layout = self.layouts[client.desktop - 1][self._currentScreen][self._currentActivity];
// 			var nextTile = layout.getAdjacentTile(activeTile, direction, true);
// 			if (nextTile != null && nextTile != activeTile) {
// 				layout.swapTiles(activeTile, nextTile);
// 			}
// 		} catch (err) {
// 			print(err, 'in TilingManager._moveTile');
// 		}
// 	}
// 
// 
// 	function _getLayouts(desktops, screens, activities) {
// 		try {
// 			if (Object.prototype.toString.call( desktops ) !== '[object Array]') {
// 				desktops=[desktops];
// 			}
// 			if (Object.prototype.toString.call( screens ) !== '[object Array]') {
// 				screens=[screens];
// 			}
// 			if (Object.prototype.toString.call( activities ) !== '[object Array]') {
// 				activities=[activities];
// 			}
// 			
// 			result=[];
// 			for (var i = 0; i < desktops.length; i++) {
// 				for (var j = 0; j < screens.length; j++) {
// 					for (var k = 0; k < activities.length; k++) {
// 						if (desktops[i] > -1) {
// 							result.push(self.layouts[desktops[i]][screens[j]][activities[k]]);
// 						} else if (desktops[i] == -1) {
// 							for (var l = 0; l < self.desktopCount; l++) {
// 								result.push(self.layouts[l][screens[j]][activities[k]]);
// 							}
// 						}
// 					}
// 				}
// 			}
// 			return result;
// 		} catch (err) {
// 			print(err, 'in TilingManager._getLayouts');
// 		}
// 	}*/

//}
